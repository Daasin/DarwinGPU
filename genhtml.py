import applegpu
from applegpu import instruction_descriptors

class Opcode(object):
	def __init__(self, bits, byte_hints=True, hint_lines=None):
		self.hint_lines = hint_lines or []
		self.byte_hints = byte_hints
		self.bits = [None] * bits
		self.colspan = [1] * bits
		self.borders = ['left right'] * bits
		self.left ='left'
		self.right = 'right'
		self.extra_row = [''] * bits
		self.extra_colspan = [1] * bits

	def add_constant(self, offset, size, value, name=None):
		assert (value & ~((1 << size) - 1)) == 0
		for i in range(size-2):
			self.borders[offset + 1 + i] = ''
		if size >= 2:
			self.borders[offset] = self.right
			self.borders[offset + size - 1] = self.left
		for i in range(size):
			assert self.bits[offset + i] is None
			self.bits[offset + i] = (value >> i) & 1

	def add_field(self, offset, size, name):
		on_extra_row = False
		for i in range(size):
			if self.bits[offset + i] is not None:
				on_extra_row = True
				break
		if on_extra_row:
			for i in range(size):
				assert not self.extra_row[offset + i]
				self.extra_row[offset + i] = name
				self.extra_colspan[offset + i] = size
		else:
			for i in range(size):
				assert self.bits[offset + i] is None
				self.bits[offset + i] = name
				self.colspan[offset + i] = size

	def to_html(self):
		for i in range(len(self.bits)-1):
			if self.bits[i] is None and self.bits[i+1] is None:
				self.borders[i] = self.borders[i].replace(self.left, '').strip()
				self.borders[i+1] = self.borders[i+1].replace(self.right, '').strip()

		parts = []
		parts.append('<div class="opcodewrapper wrapped">')

		start = 0
		for i in [16, 32, 40, 48, 64]:
			if len(self.bits) > i and i-start > 8:
				if self.bits[i] and self.bits[i-1] and self.bits[i] == self.bits[i-1]:
					pass
				else:
					parts.append(self.to_html_line(start, i))
					start = i

		if start < len(self.bits):
			parts.append(self.to_html_line(start))

		parts.append('</div>')

		parts.append('<div class="opcodewrapper notwrapped">')
		parts.append(self.to_html_line())
		parts.append('</div>')
		return ''.join(parts)

	def to_html_line(self, line_low=None, line_high=None):
		if line_low is None:
			line_low = 0
		if line_high is None:
			line_high = len(self.bits)

		start = line_high - 1
		end = line_low - 1
		step = -1

		parts = ['<table class="opcodebits">']

		parts.append('<thead><tr>')

		for i in  range(start, end, step):
			if (i+1) % 8 == 0 and self.byte_hints:
				parts.append('<td class="%s">%d</td>' % (self.left, i))
			elif i % 8 == 0 and self.byte_hints:
				parts.append('<td class="%s">%d</td>' % (self.right, i))
			elif i in self.hint_lines:
				parts.append('<td class="%s">%d</td>' % (self.left, i))
			else:
				parts.append('<td>%d</td>' % i)
		parts.append('</tr></thead>')

		parts.append('<tbody><tr>')

		o = start
		while o > end:
			i = self.bits[o]
			css_class = self.borders[o]
			if i is None:
				css_class = ('unknown ' + css_class).strip()
			parts.append('<td colspan="%d" class="%s">' % (self.colspan[o], css_class))
			if i is None:
				parts.append('?')
			elif isinstance(i, int):
				parts.append('%d' % i)
			elif isinstance(i, str):
				parts.append('%s' % i)
			parts.append('</td>')
			o -= self.colspan[o]

		parts.append('</tr>')
		parts.append('</tbody>')

		if any(self.extra_row):
			parts.append('<tfoot>')
			parts.append('<tr>')
			o = start
			while o > end:
				i = self.extra_row[o]
				css_class = 'left right' if i else ''
				parts.append('<td colspan="%d" class="%s">' % (self.extra_colspan[o], css_class))
				parts.append('%s' % i)
				parts.append('</td>')
				o -= self.extra_colspan[o]
			parts.append('</tr>')
			parts.append('</tbody>')


		parts.append('</table>')
		return ''.join(parts)


def html(s):
	return str(s).replace('&', '&amp;').replace('<', '&lt;')

for o in instruction_descriptors:
	if o.sizes[0] != o.sizes[1]:
		# if it has set opcode bits past the end of the short encoding,
		# it must be encoded with the long encoding
		if o.bits & ~((1 << (o.sizes[0] * 8)) - 1):
			o.add_constant(o.length_bit_pos, 1, 1)
		else:
			o.add_field(o.length_bit_pos, 1, 'L')

def operand_class_name(operand):
	n = operand.__class__.__name__
	if n.endswith('Desc'):
		n = n[:-len('Desc')]
	return n

def trim_indentation(s):
	s = s.replace('\t', '  ')
	lines = s.split('\n')
	indents = []
	for line in lines:
		if not line.strip():
			continue
		indents.append(len(line) - len(line.lstrip(' ')))
	m = min(indents) if indents else 0
	while lines and not lines[0].strip():
		lines.pop(0)
	while lines and not lines[-1].strip():
		lines.pop()
	return '\n'.join(i[m:] for i in lines)


for o in instruction_descriptors:
	builder = Opcode(o.sizes[-1] * 8)

	for offset, size, value in o.constants:
		builder.add_constant(offset, size, value)

	for offset, size, name in o.fields:
		builder.add_field(offset, size, name)

	name = o.name
	if hasattr(o, 'documentation_name'):
		name += ' (%s)' % (o.documentation_name,)
	print('<h3>%s</h3>' % html(name))
	if hasattr(o, 'documentation_html'):
		print(o.documentation_html)
	print(builder.to_html())

	print('<pre>')
	any_operands = False
	for operand in o.ordered_operands:
		if getattr(operand, 'documentation_skip', False):
			continue
		arguments = []
		for name, subfields in operand.merged_fields:
			arguments.append(':'.join(name for _, _, name in subfields[::-1])) #' ' + html(name) + ': ' + html(subfields))
		for _, _, name in operand.fields:
			arguments.append(name)
		arguments += getattr(operand, 'documentation_extra_arguments', [])
		if getattr(operand, 'documentation_no_name', False):
			value = ', '.join(arguments)
		else:
			value = html(operand_class_name(operand)) + '(' + ', '.join(arguments) + ')'
		print(html(operand.name) + ' = ' + value)
		any_operands = True
	if any_operands:
		print()
	if hasattr(o, 'pseudocode'):
		print(html(trim_indentation(o.pseudocode)))
	else:
		print('TODO()')
	print('</pre>')
